package com.example.gmockito.repositorytest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import com.example.gmockito.entity.Product;
import com.example.gmockito.repository.ProductRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@TestMethodOrder(OrderAnnotation.class)
public class ProductRepositoryTest {

	
	      @Autowired
	      private ProductRepository repo;
	      
	      
	      @Test
	      @Rollback(false)
	      @Order(1)
	      public void testCreateProduct() {
	    	  Product product = new Product( "iPhone 10", 80000);
	    	  Product savedProduct = repo.save(product);
	    	  
	    	assertNotNull(savedProduct);
	    	  
	      }
	      
	      
	      @Test
	      @Order(2)
	      public void  testFindProductByName() {
	    	  String name = "iPhone 10" ;
	    	 List<Product> product = repo.findByName(name);
	    	  
	    	  //assertThat(product.getName().equalsIgnoreCase(name));
	    	  
	    	 // assertThat(product.getName()).isEqualTo(name);
	      }
	      
	      @Test
	      @Order(3)
	      public void testFinProductNotExistByName() {
	    	  String name = "iPhone 12";
	    	  Product pro = repo.findNotByName(name);
	    	  assertNull(pro);
	    	  
	      }
	      @Test
	      @Rollback(false)
	      @Order(5)
	      public void testUpdateProduct() {
	    	  String name = "Realme";
	    	  Integer id=2;
	    	  
	    	  Product pr = new Product(name,1234);
	    	  pr.setId(2);
	    	  repo.save(pr);
	    	  Optional<Product> updateproduct = repo.findById(id);
	    	 // assertThat(updateproduct.get().getName()).isEqualTo(name);
	      }
	      
	      @Test
	      @Order(4)
	      public void testListProduct() {
	    	  List<Product> products = (List<Product>) repo.findAll();
	    	  
	    	  for(Product product: products) {
	    		  System.out.println(product);
	    	  }
	    	  assertThat(products).size().isGreaterThan(0);
	      }
	      
	      @Test
	      @Rollback(true)
	      @Order(6)
	     
	      
	      public void testDeleteProduct() {
	    	  Integer id = 3;
	    	  
	    	  boolean isExistBeforeDelete = repo.findById(id).isPresent();
	    	  
	    	  repo.deleteById(id);
	    	  
	    	  boolean notExistAfterDelete = repo.findById(id).isPresent();
	    	  
	    	  assertTrue(isExistBeforeDelete);
	    	  assertFalse(notExistAfterDelete);
	      }
	      
	      
	      
	      
	      
	      
	      
	      
}
