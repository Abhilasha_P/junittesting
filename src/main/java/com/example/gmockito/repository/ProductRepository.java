package com.example.gmockito.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.gmockito.entity.Product;

public interface ProductRepository  extends CrudRepository<Product ,Integer>{

	
	  public List<Product> findByName(String name);

	public Product findNotByName(String name);

	///public Product updateByName(String name);
}
