package com.example.gmockito.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="products")

public class Product {
     
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Id // primary key 
	@GeneratedValue(strategy = GenerationType.IDENTITY) //generate primary key
	private Integer id;
	
	@Column(length = 64)//maximum length
	private String name;
	
	private int price;
	
	
	public Product( String name, int id) {
		super();
		
		this.name = name;
		this.price = id;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", price=" + price + "]";
	}
	
	
	
}
